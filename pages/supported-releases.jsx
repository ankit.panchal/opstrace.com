import { withRouter } from 'next/router'
import { NextSeo } from 'next-seo'
import getCurrentUrl from 'lib/currentUrl'
import { Dots } from 'components/Patterns'

const Trademark = ({ router }) => {
  const currentUrl = getCurrentUrl(router)
  return (
    <>
      <NextSeo
        title="Opstrace Supported releases"
        description="Supported releases"
        canonical={currentUrl}
        openGraph={{
          url: currentUrl,
          title: 'Opstrace Supported Releases',
          description: ''
        }}
      />

      <div className="relative py-16 bg-white overflow-hidden">
        <div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
          <div
            className="relative h-full text-lg max-w-prose mx-auto"
            aria-hidden="true"
          >
            <Dots className="absolute top-12 left-full transform translate-x-32" />
            <Dots className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32" />
            <Dots className="absolute bottom-12 left-full transform translate-x-32" />
          </div>
        </div>
        <div className="relative px-4 sm:px-6 lg:px-8">
          <div className="text-lg max-w-prose mx-auto">
            <h1>
              <span className="block text-base text-center text-indigo-600 font-semibold tracking-wide uppercase">
                OPSTRACE
              </span>
              <span className="mt-2 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                Supported Releases
              </span>
            </h1>
            <p className="mt-8 text-xl text-gray-500 leading-8">
              Opstrace is a growing open source product. At this time we do not
              have any official release process, however we do support the
              latest versions builds off main, and encourage everyone to
              regularly update (on a weekly basis).
            </p>
          </div>
        </div>
      </div>
    </>
  )
}

export default withRouter(Trademark)
