import { withRouter } from 'next/router'
import { NextSeo } from 'next-seo'
import getCurrentUrl from 'lib/currentUrl'
import Image from 'components/Image'
import Link from 'next/link'
import { media } from 'utils/getMedia'

import { meta as page } from '../../content/pages/media.mdx'

const Blog = ({ router }) => {
  const currentUrl = getCurrentUrl(router)
  return (
    <>
      <NextSeo
        title={page.pageTitle}
        description={page.description}
        canonical={currentUrl}
        openGraph={{
          url: currentUrl,
          title: page.title,
          description: page.description
        }}
      />

      <main className="">
        <div className="container pt-16 pb-32">
          <div className="text-center">
            <h1 className="text-3xl tracking-tight font-bold sm:text-4xl mb-16">
              {page.title}
            </h1>
            <p className="mt-3 max-w-2xl mx-auto text-xl sm:mt-4">
              {page.description}
            </p>
          </div>
          <div className="relative">
            <ol className="">
              {media.map((medium, idx) => {
                return (
                  <li className="" key={`media-${idx}`}>
                    <div className="grid gap-y-6 gap-x-4 lg:gap-x-10 grid-cols-6 lg:grid-cols-12">
                      <div className="col-start-2 col-end-7 lg:col-start-1 lg:col-end-3 lg:justify-self-end lg:text-right ">
                        <time className="font-bold" dateTime={medium.date}>
                          {medium.relativeDate}
                        </time>
                      </div>
                      <div className="col-start-1 col-end-2 lg:col-start-3 lg:col-end-4 row-start-1 row-end-4 justify-self-center">
                        <svg
                          className="h-full w-2"
                          viewBox="0 0 2px 100"
                          preserveAspectRatio="none"
                        >
                          <rect
                            x="3"
                            y={idx == 0 ? '10' : '0'}
                            width="2px"
                            height="100%"
                            fill="rgb(129,140,248)"
                          />
                          <circle
                            cx="4"
                            cy="12"
                            r="4"
                            fill="rgb(129,140,248)"
                          />
                        </svg>
                      </div>

                      <div className="relative col-start-2 col-end-7 lg:col-start-4 lg:col-end-7 lg:mb-20">
                        <Link href={medium.link}>
                          <a target="_blank" rel="nofollow">
                            <Image
                              src={medium.featuredImage.src}
                              alt={medium.featuredImage.alt}
                              width="600"
                              height="400"
                              objectFit="cover"
                              layout="responsive"
                            />
                          </a>
                        </Link>
                      </div>
                      <div className="col-start-2 col-end-7 lg:col-start-7 lg:col-end-13 mb-20">
                        <p className="font-bold tracking-wider text-sm text-indigo-600 uppercase">
                          {medium.type}
                        </p>
                        <p className="text-2xl lg:text-3xl font-medium">
                          <Link href={medium.link}>
                            <a
                              className="text-blue-800"
                              target="_blank"
                              rel="nofollow"
                            >
                              {medium.title}
                            </a>
                          </Link>
                        </p>
                        <p className="text-lg break-words">
                          <medium.module.default />
                        </p>
                      </div>
                    </div>
                  </li>
                )
              })}
            </ol>
          </div>
        </div>
      </main>
    </>
  )
}

export default withRouter(Blog)
