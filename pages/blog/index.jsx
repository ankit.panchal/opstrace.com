import { withRouter } from 'next/router'
import { NextSeo } from 'next-seo'
import BlogCard from 'components/blog/BlogCard'
import BlogCardFeatured from 'components/blog/BlogCard'
import getCurrentUrl from 'lib/currentUrl'
import getPosts from 'utils/getPosts'
import Image from 'components/Image'
import readingTime from 'reading-time'
import RecommendedArticles from 'components/blog/RecommendedArticles'
import matter from 'gray-matter'
import fs from 'fs'
import { join } from 'path'

import { meta as page } from '../../content/pages/blog.mdx'

const Blog = ({ router, posts }) => {
  const currentUrl = getCurrentUrl(router)
  const [featuredPost, allPostsExceptFeatured, recommendedPosts] =
    groupPosts(posts)

  return (
    <>
      <NextSeo
        title={page.pageTitle}
        description={page.description}
        canonical={currentUrl}
        openGraph={{
          url: currentUrl,
          title: page.title,
          description: page.description
        }}
      />

      <main>
        <div className="relative overflow-x-hidden">
          {/* TODO: replace image tag to use Next's image */}
          <div className="hidden md:block absolute top-1/2 right-0 transform -translate-y-1/2 translate-x-3/4 lg:translate-x-1/2 xl:translate-x-1/3 2xl:translate-x-1/4">
            <Image
              src="/images/blog-tracy.svg"
              alt="tracy"
              width={950}
              height={140}
            />
          </div>
          <div className="container">
            <div className="py-32 relative">
              <div className="w-full md:w-2/3">
                <h1 className="text-5xl mb-4">{page.title}</h1>
                <p className="text-xl text-gray-700">{page.description}</p>
              </div>
            </div>
          </div>
        </div>
        <div className="xl:pt-16 pb-32 lg:container lg:grid grid-cols-7 gap-32">
          <div className="col-start-1 col-end-6">
            <div className="px-1/20 lg:px-0">
              <BlogCardFeatured
                className="mb-16 lg:mb-32"
                key={featuredPost.slug}
                post={featuredPost}
              />
            </div>
            <RecommendedArticles
              className="lg:hidden px-1/20 py-10 mb-20 bg-blue-100"
              posts={recommendedPosts}
            />
            <div className="px-1/20 lg:px-0">
              {allPostsExceptFeatured.map((post) => (
                <BlogCard className="mb-24" key={post.slug} post={post} />
              ))}
            </div>
          </div>
          <div className="hidden lg:block col-start-6 col-end-8">
            <RecommendedArticles
              className="sticky top-4 mb-24"
              posts={recommendedPosts}
            />
          </div>
        </div>
      </main>
    </>
  )
}

function groupPosts(posts) {
  const articles = posts
    .filter((post) => post && post.type && post.type.length > 0)
    .filter((post) => post.type.toLowerCase() === 'article')
  articles.sort((a, b) => {
    return (b.score || 0) - (a.score || 0) || b.date - a.date
  })

  const recommendedPosts = articles.slice(1, 11)
  const featuredPost = articles.slice(0, 1)[0]
  const allPostsExceptfeatured = posts.filter((post) => post !== featuredPost)
  return [featuredPost, allPostsExceptfeatured, recommendedPosts]
}

export default withRouter(Blog)

export async function getStaticProps() {
  const posts = ((context) => {
    return getPosts(context)
  })(require.context('content/articles', true, /\.\/.*\.mdx$/))

  for (const post of posts) {
    const fileContents = fs.readFileSync(
      join(process.cwd(), 'content', 'articles', `${post.id}.mdx`),
      'utf8'
    )
    const { content } = matter(fileContents)
    const rt = readingTime(content)
    post.readingTime = rt
  }

  return {
    props: {
      posts
    }
  }
}
