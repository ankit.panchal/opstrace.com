import { withRouter } from 'next/router'
import Link from 'next/link'
import { NextSeo } from 'next-seo'
import getCurrentUrl, { getDeployUrl } from 'lib/currentUrl'
import Image from 'components/Image'
import authors from 'content/team'
import readingTime from 'reading-time'
import getSlugs from 'utils/getSlugs'
import matter from 'gray-matter'
import fs from 'fs'
import { join } from 'path'
import { SRLWrapper } from 'simple-react-lightbox'
import { serialize } from 'next-mdx-remote/serialize'
import { MDXRemote } from 'next-mdx-remote'
import getPosts from 'utils/getPosts'
import ArticleHeader from 'components/blog/ArticleHeader'
import Authors from 'components/blog/Authors'
import CodeBlock from 'components/CodeBlock'
import InlineCode from 'components/InlineCode'
import RecommendedArticles from 'components/blog/RecommendedArticles'
import rehypeSlug from 'rehype-slug'
import rehypeAutolink from 'rehype-autolink-headings'
import styles from 'styles/blog.module.scss'
import ArticleFooter from 'components/blog/ArticleFooter'

const BlogPost = ({ router, mdxSource, meta, posts }) => {
  const currentUrl = getCurrentUrl(router)
  const deployUrl = getDeployUrl()
  const components = {
    Image,
    Link,
    // eslint-disable-next-line react/display-name
    pre: (props) => <div {...props} />,
    code: CodeBlock,
    inlineCode: InlineCode
  }
  const postAuthors = meta.authors.map((key) => authors[key])
  const recommended = recommendedPosts(posts, meta.slug)

  return (
    <>
      <NextSeo
        title={meta.title}
        description={meta.description}
        canonical={currentUrl}
        openGraph={{
          url: currentUrl,
          title: meta.title,
          description: meta.description,
          images: [
            {
              url: `${deployUrl}${meta.featuredImage.src}`,
              alt: meta.featuredImage.alt
            }
          ]
        }}
      />
      <div className='h-20'></div>
      <div className="container py-20 lg:grid grid-cols-10 gap-16">
        <div className="hidden xl:block col-span-2">
          <Authors authors={postAuthors} expanded={true} />
        </div>
        <main className="col-span-12 lg:col-span-8">
          <div className="xl:grid grid-cols-8 gap-16">
            <article className="col-span-8 xl:col-span-6">
              <div className="">
                <ArticleHeader meta={meta} authors={postAuthors} />
                <div className={styles.blog}>
                  <SRLWrapper>
                    <MDXRemote {...mdxSource} components={components} />
                  </SRLWrapper>
                </div>
              </div>
              <ArticleFooter
                recommended={recommended.slice(0, 5)}
                currentPath={router.asPath}
              />
            </article>
            <aside className="hidden lg:block col-span-2">
              <RecommendedArticles posts={recommended} />
            </aside>
          </div>
        </main>
      </div>
    </>
  )
}

function recommendedPosts(posts, currentSlug) {
  const articles = posts
    .filter((post) => post && post.type && post.type.length > 0)
    .filter(
      (post) =>
        post.type.toLowerCase() === 'article' && post.slug !== currentSlug
    )
  articles.sort((a, b) => {
    return (b.score || 0) - (a.score || 0) || b.date - a.date
  })
  return articles.slice(0, 10)
}

export default withRouter(BlogPost)

export async function getStaticProps({ ...ctx }) {
  const posts = ((context) => {
    return getPosts(context)
  })(require.context('content/articles', true, /\.\/.*\.mdx$/))

  const { postname } = ctx.params
  const fileContents = fs.readFileSync(
    join(process.cwd(), 'content', 'articles', `${postname}.mdx`),
    'utf8'
  )
  const { data, content } = matter(fileContents)
  const mdxSource = await serialize(content, {
    scope: { data },
    mdxOptions: {
      target: ['es2020'],
      rehypePlugins: [
        rehypeSlug,
        [
          rehypeAutolink,
          {
            behavior: 'append',
            properties: {
              className: ['flex', 'invisible']
            },
            content: {
              type: 'element',
              tagName: 'svg',
              properties: {
                className: ['h-6', 'w-6', 'mt-2', 'ml-2', 'text-pink-600'],
                xmlns: 'http://www.w3.org/2000/svg',
                fill: 'none',
                viewBox: '0 0 24 24',
                stroke: 'currentColor'
              },
              children: [
                {
                  type: 'element',
                  tagName: 'path',
                  properties: {
                    strokeLinecap: 'round',
                    strokeLinejoin: 'round',
                    strokeWidth: 2,
                    d: 'M13.828 10.172a4 4 0 00-5.656 0l-4 4a4 4 0 105.656 5.656l1.102-1.101m-.758-4.899a4 4 0 005.656 0l4-4a4 4 0 00-5.656-5.656l-1.1 1.1'
                  },
                  children: []
                }
              ]
            }
          }
        ]
      ]
    }
  })

  const { meta } = await import(`content/articles/${postname}`)

  const rt = readingTime(content)

  const articleAuthors = meta.authors.map((a) => {
    return authors[a]
  })
  meta.authorNames = articleAuthors.map((a) => a.name).join(', ')
  meta.readingTime = rt
  return {
    props: {
      mdxSource,
      postname,
      meta,
      posts
    }
  }
}

export async function getStaticPaths() {
  const blogSlugs = ((context) => {
    return getSlugs(context)
  })(require.context('content/articles', true, /\.\/.*\.mdx$/))

  const paths = blogSlugs.map((slug) => `/blog/${slug}`)

  return {
    paths, // An array of path names, and any params
    fallback: false // so that 404s properly appear if something's not matching
  }
}
