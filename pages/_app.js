import '../styles/index.scss'
import SEO from '../next-seo.config'
import Navigation from 'components/Navigation'
import Footer from 'components/Footer'
import siteContent from '../content/site.js'
import { DefaultSeo } from 'next-seo'
import SimpleReactLightbox from 'simple-react-lightbox'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <DefaultSeo {...SEO} />
      <Navigation />
      <SimpleReactLightbox>
        <Component {...pageProps} />
      </SimpleReactLightbox>
      <Footer footer={siteContent.footer} />
    </>
  )
}

export default MyApp
