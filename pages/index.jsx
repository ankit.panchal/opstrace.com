import { withRouter } from 'next/router'
import Img from 'components/Image'
import { NextSeo } from 'next-seo'
import getCurrentUrl from 'lib/currentUrl'
import { meta as page } from '../content/pages/homepage.mdx'
import readingTime from 'reading-time'
import matter from 'gray-matter'
import fs from 'fs'
import { join } from 'path'
import getPosts from 'utils/getPosts'
import LinkButton from 'components/LinkButton'
import BlogCard from 'components/BlogCard'

const Home = ({ router, posts }) => {
  const currentUrl = getCurrentUrl(router)
  const featuredPost = posts.find((post) => post.featured) || posts[0]
  const remainingPosts = posts.filter(
    (post) => post.title !== featuredPost.title
  )
  return (
    <>
      <NextSeo
        title={page.pageTitle}
        description={page.description}
        canonical={currentUrl}
        openGraph={{
          url: currentUrl,
          title: page.title,
          description: page.description
        }}
      />
      <main>
        <div
          style={{ backgroundImage: process.env.NODE_ENV === 'production' ? "url('/opstrace.com/images/gradient-bg.svg')" :  "url('/images/gradient-bg.svg')" }}
          className="relative overflow-hidden bg-no-repeat bg-center bg-cover pt-20 pb-32"
        >
          <div className="h-20"></div>
          <div className="container">
            <div className="grid grid-cols-1 lg:grid-cols-2 items-center gap-14">
              <div>
                <h1 className="font-semibold text-6xl leading-tight mb-8">
                  {featuredPost.title}
                </h1>
                <p className="opacity-80 text-lg mb-7">
                  {featuredPost.description}
                </p>
                <LinkButton text="Read announcement" href={featuredPost.slug} />
              </div>
              <div className="">
                <Img
                  className="w-full"
                  src="/images/application-infrastructure.svg"
                  alt="Overview of how Opstrace works"
                  width="729"
                  height="547"
                  layout="intrinsic"
                />
              </div>
            </div>
          </div>
          <div className="absolute top-full right-1/2 transform  translate-x-40 -translate-y-3/4">
            <Img
              src="/images/tracy-waving.svg"
              alt="Tracy waving illustration"
              width="92"
              height="97"
              layout="intrinsic"
            />
          </div>
        </div>

        <div className="container pt-20 pb-32">
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
            {remainingPosts.map((post) => (
              <BlogCard post={post} key={post.slug} />
            ))}
          </div>
        </div>
      </main>
    </>
  )
}

export default withRouter(Home)

export async function getStaticProps() {
  const posts = ((context) => {
    return getPosts(context)
  })(require.context('content/articles', true, /\.\/.*\.mdx$/))

  for (const post of posts) {
    const fileContents = fs.readFileSync(
      join(process.cwd(), 'content', 'articles', `${post.id}.mdx`),
      'utf8'
    )
    const { content } = matter(fileContents)
    const rt = readingTime(content)
    post.readingTime = rt
  }

  return {
    props: {
      posts
    }
  }
}
