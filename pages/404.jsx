import Head from 'next/head'
import Link from 'next/link'

const NotFound = () => {
  return (
    <div>
      <Head>
        <title>Opstrace - Page Not Found</title>
        <meta name="description" content="Page Not Found." />
      </Head>
      <div className="container text-center">
        <img
          className="mt-2 w-1/32 lg:w-1/32 mx-auto"
          src="/tracy/tracy-searching-v8.svg"
        />
        <h1 className="text-3xl md:text-5xl font-bold text-opstrace-600 mt-10 lg:mt-20">
          Page not found
        </h1>
        <p className="mt-4">
          Check out our{' '}
          <Link href="/">
            <a>home page</a>
          </Link>{' '}
          or{' '}
          <Link href="/docs">
            <a>docs</a>
          </Link>
          .
        </p>
      </div>
    </div>
  )
}

export default NotFound
