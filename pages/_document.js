import Document, { Html, Head, Main, NextScript } from 'next/document'

class Opstrace extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <link
            rel="preload"
            href="https://fonts.googleapis.com/css2?family=Open%20Sans:wght@300;400;600;700;800&display=swap"
            as="style"
            crossOrigin="anonymous"
          />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css2?family=Open%20Sans:wght@300;400;600;700;800&display=swap"
            crossOrigin="anonymous"
          />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon-16x16.png"
          />
          <link
            rel="alternate"
            type="application/rss+xml"
            title="Opstrace RSS2 Feed"
            href="https://opstrace.com/feed.xml"
          />
          <link
            rel="alternate"
            type="application/atom+xml"
            title="Opstrace Atom Feed"
            href="https://opstrace.com/atom.xml"
          />
          <link
            rel="alternate"
            type="application/json"
            title="Opstrace JSON Feed"
            href="https://opstrace.com/feed.json"
          />
          <link rel="manifest" href="/site.webmanifest" />
          <link rel="shortcut icon" href="/favicon.ico" />
          <link rel="icon" href="/favicon.ico" />
          <script async defer src="https://buttons.github.io/buttons.js" />
        </Head>
        <body className="antialiased font-sans bg-white">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default Opstrace
