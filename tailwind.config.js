const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  darkMode: false,
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    purgeLayersByDefault: true,
    content: ['./pages/**/*.jsx', './components/**/*.jsx']
  },
  theme: {
    extend: {
      container: {
        center: true
      },
      typography: {
        DEFAULT: {
          css: {
            'code::before': {
              content: '""'
            },
            'code::after': {
              content: '""'
            }
          }
        }
      },
      colors: {
        special: {
          100: '#3b4583'
        },
        opstrace: {
          400: '#646DA5',
          500: '#4C57A6',
          600: '#091570',
          800: '#01083D',
          900: '#3b4283'
        },
        blue: {
          400: '#7885EB',
          600: '#ECEDFD',
          700: '#05105D',
          800: '#273073',
          900: '#07104E'
        },
        pink: {
          400: '#F98CA8',
          600: '#F54773',
          800: '#D42D57'
        },
        gray: {
          100: '#E6E6E6',
          200: '#CCCCCC',
          700: '#4c5173',
          800: '#4D4D4D',
          900: '#1A1A1A'
        },
        black: '#1A1A1A'
      },
      fontFamily: {
        sans: ['Barlow', ...defaultTheme.fontFamily.sans]
      },
      backgroundImage: {
        bubblesShort: "url('/backgrounds/bubbles-wide-short-1920px.svg')",
        bubblesWide: "url('/backgrounds/bubbles-wide-1920px.svg')",
        bubblesBw: "url('/backgrounds/bubbles_white-wide-1920px.svg')",
        wavesLight: "url('/backgrounds/waves_light-wide-1920px.svg')",
        tracySwimming: "url('/tracy/tracy_swimming-percent.svg')",
        tracyThinking: "url('/tracy/tracey_newsletter-all-690px.svg')",
        wavesWithTracy:
          "url('/backgrounds/waves_and_tracey-narrowest_a-short_c-right-640px.svg')",
        wavesWithTracySM:
          "url('/backgrounds/waves_and_tracey-narrowest_b-right-768px.svg')",
        wavesWithTracyMD:
          "url('/backgrounds/waves_and_tracey-narrowest_c-right-1024px.svg')",
        wavesWithTracyLG:
          "url('/backgrounds/waves_and_tracey-narrowest_d-right-1280px.svg')",
        wavesWithTracyXL:
          "url('/backgrounds/waves_and_tracey-widest_b-right-4096px.svg')",
        wavesBottom: "url('/backgrounds/waves_top-narrow-1440px.svg')"
      },
      transitionProperty: {
        height: 'height'
      }
    }
  },
  variants: {
    extend: {
      width: ['hover'],
      borderWidth: ['hover'],
      backgroundColor: ['active']
    }
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')]
}
