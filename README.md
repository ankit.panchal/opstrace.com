# opstrace.com

This is the source for the https://opstrace.com/ site.

## Installation & Use

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the
result.

To deploy to production, open a PR to merge `main` into `production`.

## SEO / Social Previews

We use [Next SEO](https://github.com/garmeeh/next-seo). Each page has
`<NextSEO>` in the page header with specific instructions for the title, image
etc.
