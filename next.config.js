const slug = require('rehype-slug')
const link = require('rehype-autolink-headings')

const withMDX = require('@next/mdx')({
  extension: /\.mdx$/,
  options: {
    remarkPlugins: [],
    rehypePlugins: [slug, link]
  }
})

module.exports = withMDX({
  basePath: '/opstrace.com', 
  assetPrefix: process.env.NODE_ENV === 'production' ? '/opstrace.com/' : '',
  reactStrictMode: true,
  images: {
    domains: ['share.balsamiq.com'],
    loader: 'imgix',
    path: ''
  },
  pageExtensions: ['js', 'jsx', 'mdx'],
  webpack: function (config, { dev, isServer }) {
    if (!dev && isServer) {
      // const originalEntry = config.entry
      // config.entry = async () => {
      //   const entries = { ...(await originalEntry()) }
      //   entries['utils/generate-rss.js'] = 'utils/generate-rss.js'
      //   return entries
      // }
    }

    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader'
    })
    config.module.rules.push({
      test: /\.yml$/,
      use: 'raw-loader'
    })
    config.resolve.extensions = [...config.resolve.extensions, '.md', '.mdx']
    return config
  }
})
