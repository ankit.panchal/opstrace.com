export default {
  openGraph: {
    type: 'website',
    locale: 'en_EN',
    url: `https://opstrace.com/`,
    site_name: 'Opstrace',
    images: [
      {
        url: `https://opstrace.com/images/opstrace-social.png`,
        width: 1200,
        height: 628,
        alt: 'Opstrace Logo'
      }
    ]
  },
  twitter: {
    handle: '@opstrace',
    site: '@site',
    cardType: 'summary_large_image'
  }
}
