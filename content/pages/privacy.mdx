export const meta = {
  pageTitle: 'Opstrace - Privacy Policy',
  heading: 'Privacy Policy',
  description: ''
}

Last updated: March 10, 2021

Opstrace, Inc. (“**Opstrace**”, “**we**”, “**us**”, “**our**”) knows how
important your privacy is. This document governs our privacy policies and
procedures with respect to our website (the “**Site**”) and our products and
services, including our managed observability platform and related services
(collectively, our “**Services**”). This Privacy Policy sets forth how we
collect, use, disclose, and otherwise process Personal Information in connection
with our Site and our Services. If you have any questions about how we handle
your information, you can email privacy@opstrace.com.

## What Is Personal Information?

When we use the term “**Personal Information**” in this Privacy Policy, we mean
information that identifies, relates to, describes, is reasonably capable of
being associated with, or could reasonably be linked, directly or indirectly, to
an individual. It does not include aggregated or deidentified information that
is maintained in a form that is not reasonably capable of being associated with
or linked to an individual.

## Information We Collect

When you interact with us through the Site or the Services, we may collect
Personal Information and other information from you, as further described below.
Sometimes we collect Personal Information automatically when an individual
interacts with our Site and Services, and sometimes, we collect the Personal
Information directly from an individual. In some circumstances, we may obtain
information from third parties before we have interacted with you directly (such
as information that is publicly available on your employer’s website or that you
post on social media). We collect Personal Information about different
categories of individuals through our Site and Services. These include our
customers and prospective customers (including representatives of our business
customers), partners and prospective partners, and visitors to our websites.

### General

We collect Personal Information in connection with our Site and Services from
our customers, prospective customers, partners and prospective partners:

- **Information provided to us directly**, such as name, phone number, email
  address, location (including city, state or region), language information,
  payment account information, current employment information (such as company
  name, industry and professional title), profile image and other profile
  information, and any other information you choose to provide or that a
  business partner or prospective partner shares with us.
- **Data collected through the use of the Site and** **Services**, such as how
  an individual uses the Site and Services and their actions on the Site and
  Services, which may include information that an individual provides to us when
  they contact us with inquiries, respond to a survey we run, register for a
  webinar, or access our Services. Such information may include any third-party
  application profile data that you permit to be made available to us.
- **Inquiry Information**, including the content of emails you send to us or
  comments you post on our Site;
- **Registration Information**, such as your log-in details [including your user
  name and password].
- **Personal Information Automatically Collected**: As is true of most digital
  platforms, we collect certain Personal Information automatically when you
  visit our Site or Services, including:
  - **Log File Data**, including internet protocol (IP) address, operating
    system, browser type, browser id, date/time of visit, and pages visited.
  - **Analytics Data**, such as the electronic path taken to our Site, through
    our Site and when exiting our Site, as well as usage and activity on our
    Site and Services, such as the links and objects viewed, clicked or
    otherwise interacted with (also known as “**Clickstream Data**”).
  - **Location Data**, including general geographic location based on IP address
    or more precise location when accessing our Site or Services through a
    mobile device.

For information about our and our third-party partners’ use of cookies and
related technologies to collect information automatically, and any choices
individuals may have in relation to its collection, please refer to the
[Cookies](#cookies) section of this Privacy Policy below.

#### Personal Information from Third Parties

We also obtain Personal Information from other sources, which we often combine
with Personal Information we collect either automatically or directly from an
individual.

We receive the same categories of Personal Information as described above from
the following sources and third parties:

- **Service Providers**: Our service providers that perform services solely on
  our behalf, such as payment processors and analytics providers. For example,
  we receive Personal Information from our service providers that conduct
  analytics activities.
- **Business Partners**: Our business partners may collect Personal Information
  in connection with the Site and Services and may share some or all of the
  information with us.
- **Publicly Available Sources**: We collect Personal Information about
  individuals that we do not otherwise have, including contact information,
  employment-related information, and interest information, from publicly
  available sources. We may combine this information with the information we
  collect from an individual directly.
- **Social Networks**: When an individual interacts with us through social media
  networks, such as when someone follows us or shares our content on Twitter,
  LinkedIn or other social media networks, we may receive some information about
  the individual that the individual permits the social network to share. The
  data we receive depends on the individual’s privacy settings with the social
  network. Before linking or connecting with us on a social media network, you
  should always review and, if necessary, adjust your privacy settings on that
  network in accordance with your preferences.
- **Other Third Parties**: From time to time, we may receive information about
  individuals from third parties. For example, we may obtain information from
  marketing partners or from third parties to enhance or supplement our existing
  information about an individual. We may also collect information about
  individuals that is publicly available. We may combine this information with
  the information we collect directly from individuals.

## Our Use of Personal Information

We use Personal Information we collect to:

- Fulfill or meet the reason for which the Personal Information was provided.
  For instance, if you contact us by email, we will use the Personal Information
  you provide to answer your question or resolve your problem.
- Communicate with individuals, including via email, push notification, social
  media and/or telephone calls;
- Provide you with access to the requested product or services and to monitor
  your use of such product or services;
- Operate, maintain and provide the features and functionality of the Site and
  Services;
- Help us improve the content and functionality of the Site and Services, to
  better understand our users and to improve the Site and Services;
- Market to individuals or contact individuals in the future to tell them about
  Services we believe will be of interest to them. If we do so, each marketing
  communication we send you will contain instructions permitting you to
  “opt-out” of receiving future marketing communications. In addition, if at any
  time you wish not to receive any future marketing communications or you wish
  to have your name deleted from our mailing lists, please contact us as
  indicated below;
- Help maintain the safety, security and integrity of our Site and Services,
  technology assets and business;
- Improve, test, enhance, update and monitor the Site and Services or to
  diagnose or fix technology problems;
- Respond to law enforcement requests and as required by applicable law, court
  order or governmental regulations;
- Defend, protect or enforce our rights or applicable terms of use;
- Detect and prevent fraud;
- Evaluate, negotiate or conduct a merger, divesture, restructuring,
  reorganization, dissolution, or other sale or transfer of some or all of
  Opstrace’s assets, whether as a going concern or as a part of bankruptcy,
  liquidation or similar proceeding, in which Personal Information held by
  Opstrace about consumers is among the assets transferred or is otherwise
  relevant to the evaluation, negotiation or conduct of the transaction;
- Comply with or as otherwise permitted by law.

Where an individual chooses to contact us, we may need additional information to
fulfill the request or respond to the inquiry. We may provide additional privacy
disclosures where the scope of the inquiry/request and/or Personal Information
we require fall outside the scope of this Privacy Policy. In that case, the
additional privacy disclosures will govern how we may process the information
provided at that time.

## Our Disclosure of Personal Information

We disclose your Personal Information as set forth below:

**Within Opstrace**: Opstrace entities may disclose Personal Information to
other Opstrace entities for purposes and uses consistent with this Privacy
Policy.

**Business Partners**: We may share customer Personal Information with our
business partners, such as our online platform store and commerce solutions
partners, as well as third parties with whom we partner to provide contests,
joint promotional activities or co-branded services, and such disclosure is
necessary to fulfill requests or applications.

**Service Providers**: Opstrace, like many businesses, sometimes hires other
companies to perform certain business-related functions, subject to contractual
terms restricting the collection, use and disclosure of Personal Information for
any other commercial purpose. Examples of such functions include mailing
information, maintaining databases and processing payments.

**Your Employer**: If you are using the Site or Services in connection with your
role as an employee or contractor of a company or other legal entity, we may
share your information with such entity.

**Third Parties at the Direction of the Individual**: We share Personal
Information with third parties with your consent or at your direction.

**Other Third Parties**: We may disclose Personal Information to third parties
who use this information to provide information or marketing messages about
products or services of interest, in accordance with their own privacy policies
and terms. We also work with analytics and social media partners as described in
the [Cookies](#cookies) section below.

**Legal Requirements**: Opstrace may disclose your Personal Information if
required to do so by law or in the good faith belief that such action is
necessary to (i) comply with a legal obligation or with applicable law, court
order, or governmental regulation, (ii) protect and defend the rights or
property of Opstrace, (iii) act in urgent circumstances to protect the personal
safety of users of the Services or the public, or (iv) protect against legal
liability.

**Third Parties in the Context of a Corporate Transaction**: We may disclose
Personal Information as reasonably necessary to evaluate, negotiate or conduct a
merger, divestiture, restructuring, reorganization, dissolution, or other sale
or transfer of some or all of Opstrace assets, whether as a going concern or as
part of bankruptcy, liquidation, or similar proceeding, in which Personal
Information held by Opstrace about consumers is among the assets transferred or
is otherwise relevant to the evaluation, negotiation or conduct of the
transaction.

Our Services currently do not respond to “Do Not Track” (DNT) signals and
operate as described in this Privacy Policy whether or not a DNT signal is
received.

## Cookies

### What We Collect

We, and our third-party partners, may automatically collect certain types of
usage information when an individual visits our Site or interacts with our
Services. We may collect this information through a variety of data collection
technologies, including cookies, web beacons, embedded scripts,
location-identifying technologies, file information, and similar technology
(collectively, “**cookies**”). For example, we may collect information about an
individual’s device and its software, such as IP address, browser type, Internet
service provider, platform type, device type, operating system, date and time
stamp, and other similar information when the individual accesses our Site. We
may collect analytics data or use third-party analytics tools such as Segment,
Intercom, Google Analytics and Hotjar to help us measure traffic and usage
trends for our Site and to understand more about the demographics of our users.

### How We Use That Information

We use or may use the data collected through cookies to: (a) remember
information so that an individual will not have to re-enter it during their
visit or the next time they use our Site or Services; (b) provide and monitor
the effectiveness of our Site and Services; (c) monitor aggregate metrics such
as total number of visitors, traffic, usage, and demographic patterns on our
Site and Services; (d) diagnose or fix technology problems; or (e) otherwise to
plan for and enhance our Services.

### Choices About Cookies

To manage cookies, an individual may change their browser settings to: (i)
notify them when they receive a cookie, so the individual can choose whether or
not to accept it; (ii) disable existing cookies; or (iii) set their browser to
automatically reject cookies. Please note that doing so may negatively impact an
individual’s experience using our Site or Services, as some features may not
work properly. Depending on an individual’s device and operating system, they
may not be able to delete or block all cookies. In addition, if an individual
wants to reject cookies across all their browsers and devices, they will need to
do so on each browser on each device they actively use. An individual may also
set their email options to prevent the automatic downloading of images that may
contain technologies that would allow us to know whether they have accessed our
email and performed certain functions with it.

#### We use the following types of cookies

- **Strictly necessary cookies**. These cookies enable core functionality such
  as security, network management and accessibility. You may disable these by
  changing your browser settings, but this may affect how the Site and Services
  function. The legal basis for our use of strictly necessary cookies is our
  legitimate interests, namely being able to provide and maintain our Site and
  Services. Please see the [Annex](#annex---cookies) below for more information.
- **Functionality cookies**. These enable a website to remember information that
  changes the way the website behaves or looks, like your preferred language or
  the region that you are in. The legal basis for our use of functionality
  cookies is our legitimate interests, namely being able to provide and maintain
  our Site and Services. Please see the [Annex](#annex---cookies) below for more
  information.
- **Analytical/performance cookies**. These cookies allow us to recognize and
  count the number of visitors to our Site and Services, and to see how visitors
  move around our Site and Services when they are using them. This helps us to
  improve the way our Site and Services work, for example, by ensuring that
  users are finding what they are looking for easily. The legal basis we rely on
  for these types of cookies is consent. You are free to deny your consent.
  Please see the [Annex](#annex---cookies) below for more information.

## Children

Our Site and Services are not directed to, and Opstrace does not intend to or
knowingly collect or solicit Personal Information from children under the age of
13\. If you are under the age of 13, please do not submit any Personal
Information through or use the Site or Services. We encourage parents and legal
guardians to monitor their children’s Internet usage and to help enforce our
Privacy Policy by instructing their children never to provide Personal
Information on the Site or Services without their permission. If you have reason
to believe that a child under the age of 13 has provided Personal Information to
Opstrace through the Site or Services, please contact us, and we will endeavor
to delete that information from our databases.

## What This Policy Does Not Cover

This Privacy Policy applies only to the Site and our Services. The Site and
Services may contain links to other web sites not operated or controlled by
Opstrace (the “**Third Party Sites**”). The policies and procedures described
here do not apply to the Third Party Sites. The links from the Site and Services
do not imply that Opstrace endorses or has reviewed the Third Party Sites. We
suggest contacting those sites directly for information on their privacy
policies.

This Privacy Policy also does not cover or address: services that we provide
solely on behalf of a third party, our Personal Information and privacy
practices relating to job applicants, our own employees and other personnel.

## Security

We take security seriously. If you believe you have found a security issue in
our Site or Services, please email us at
[security@opstrace.com](mailto:security@opstrace.com) to responsibly disclose
the issue.

Opstrace takes reasonable steps to protect the Personal Information provided via
the Site or Services from loss, misuse, and unauthorized access, disclosure,
alteration, or destruction. However, no Internet or email transmission is ever
fully secure or error free. Therefore you should take special care when
disclosing any Personal Information to Opstrace via the Internet or by email.

## Region-Specific Disclosures

We may choose or be required by law to provide different or additional
disclosures relating to the processing of Personal Information about residents
of certain countries, regions or states. Please refer below for disclosures that
may be applicable to you:

- For residents of the State of Nevada in the United States, Chapter 603A of the
  Nevada Revised Statutes permits Nevada residents to opt out of future sales of
  certain covered information that a website operator has collected or will
  collect about the resident. To submit such a request, please contact us at
  [privacy@opstrace.com](mailto:privacy@opstrace.com).
- If you are based in the European Economic Area (“**EEA**”), Switzerland or the
  United Kingdom (“**UK**”), please click [here](/privacy-gdpr-supplement) for
  additional European-specific privacy disclosures.

## Changes to Opstrace’s Privacy Policy

The Site, the Services and other aspects our business may change from time to
time. As a result, at times it may be necessary for Opstrace to make changes to
this Privacy Policy. Opstrace reserves the right to update or modify this
Privacy Policy at any time and from time to time without prior notice. If we
make material changes to this Privacy Policy, we may provide notice by email, by
prominently posting on this Site or our Services, or through other appropriate
communication channels. All changes shall be effective from the date of
publication unless otherwise provided. Please review this policy periodically,
and especially before you provide any Personal Information. This Privacy Policy
was last updated on the date indicated above. Your continued use of the Site or
Services after any changes or revisions to this Privacy Policy shall indicate
your agreement with the terms of such revised Privacy Policy.

## Access to Information; Contacting Opstrace

To keep your Personal Information accurate, current, and complete, please
contact us at [privacy@opstrace.com](mailto:privacy@opstrace.com). We will take
reasonable steps to update or correct Personal Information in our possession
that you have previously submitted via the Site and Services.

Please also feel free to contact us at
[privacy@opstrace.com](mailto:privacy@opstrace.com) if you have any questions
about Opstrace’s Privacy Policy or the information practices of the Site and
Services.

Alternatively, inquiries may be addressed to:

Opstrace, Inc.  
Attn: Privacy  
2261 Market Street #4240  
San Francisco CA, 94114  
United States

---

## Annex - Cookies

<table>
  <thead>
    <tr>
      <th className="w-2/12">Cookie Name</th>
      <th className="w-2/12">Type of cookie</th>
      <th className="w-2/12">Used On</th>
      <th className="w-2/12">Purposes</th>
      <th className="w-4/12">Additional description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Segment</td>
      <td>Analytics cookie</td>
      <td>
        <a href="https://opstrace.com">https://opstrace.com</a>
      </td>
      <td>Event data collection and routing.</td>
      <td>
        For further details, please see Segment’s Website Data Collection Policy
        which can be found here:
        <a href="https://segment.com/legal/website-data-collection-policy/">
          https://segment.com/legal/website-data-collection-policy/
        </a>. The Policy includes additional information about how to change your
        preferences.
      </td>
    </tr>
    <tr>
      <td>Hotjar</td>
      <td>Analytics cookie</td>
      <td>
        <a href="https://opstrace.com">https://opstrace.com</a>
      </td>
      <td>Visitor usage heatmaps.</td>
      <td>
        For further details, please see Hotjar’s privacy policy which can be
        found here:{' '}
        <a href="https://www.hotjar.com/legal/policies/privacy">
          https://www.hotjar.com/legal/policies/privacy
        </a>
        . You can opt-out of Hotjar supported analytics and data collection by visiting:{' '}
        <a href="https://www.hotjar.com/legal/compliance/opt-out">
          https://www.hotjar.com/legal/compliance/opt-out
        </a>.
      </td>
    </tr>
    <tr>
      <td>Intercom</td>
      <td>Analytics cookie</td>
      <td>
        <a href="https://opstrace.com">https://opstrace.com</a>
      </td>
      <td>Visitor and customer chat and other data gathering interactions.</td>
      <td>
        For more information on Intercom&aops;s use of cookies, please visit
        <a href="https://www.intercom.com/legal/cookie-policy">
          https://www.intercom.com/legal/cookie-policy
        </a>. For more information on the privacy practices of Intercom and to express
        your privacy preferences, please visit <a href="https://www.intercom.com/legal/privacy">
          https://www.intercom.com/legal/privacy
        </a>.
      </td>
    </tr>
    <tr>
      <td>Google</td>
      <td>Analytics cookie</td>
      <td>
        <a href="https://opstrace.com">https://opstrace.com</a>
      </td>
      <td></td>
      <td>
        This is one of the four main Cookies set by the Google Analytics
        services which enables website owners to understand visitor behavior and
        measure the website performance. This Cookie distinguishes between users
        and sessions. It is used to calculate new and returning visitor
        statistics. The Cookie is updated every time data is sent to Cookie
        Analytics. The lifespan of the Cookie can be customized by website
        owners. Currently configured for six months.
      </td>
    </tr>
  </tbody>
</table>

Please also see Google’s Policy:
https://policies.google.com/technologies/cookies?hl=en | You may review Google's
site “How Google uses data when you use our partners’ sites or apps” located at
https://www.google.com/policies/privacy/partners/. You can learn about Google
Analytics’ currently available opt-outs, including the Google Analytics Browser
Ad-On here: https://tools.google.com/dlpage/gaoptout/
