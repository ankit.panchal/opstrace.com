const year = new Date().getFullYear()

const meta = {
  title: 'Opstrace - The Only Open Source Observability Distribution',
  link: 'https://opstrace.com',
  description: 'Opstrace is the only open source observability distribution.',
  language: 'en',
  image: 'https://opstrace.com/images/opstrace-social.png',
  favicon: 'https://opstrace.com/favicon.ico',
  copyright: `© ${year} Opstrace, Inc.`,
  footer: {
    slogan: 'The Open Source Observability Distribution',
    columns: [
      {
        title: 'Company',
        links: [
          {
            text: 'Contact us',
            href: 'mailto:hello@opstrace.com'
          },
          {
            text: 'Blog',
            href: '/blog'
          }
        ]
      },
      {
        title: 'Product',
        links: [
          {
            text: 'Open Source on GitHub',
            href: 'https://github.com/opstrace/opstrace'
          },
          {
            text: 'Roadmap',
            href: '/docs/references/roadmap'
          }
        ]
      },
      {
        title: 'Resources',
        links: [
          {
            text: 'Docs',
            href: '/docs'
          },
          {
            text: 'In the Media',
            href: '/media'
          }
        ]
      }
    ],
    newsletterFooter: {
      version: 'footer',
      title: 'Sign up for updates as we grow',
      text: "We won't share your information with anyone",
      button: {
        text: 'Notify me'
      }
    }
  }
}

export default meta
