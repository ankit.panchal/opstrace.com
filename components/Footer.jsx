import Link from 'next/link'
import Image from 'components/Image'

export default function Footer() {
  return (
    <div className="mt-32">
      <footer className="relative bg-black text-white py-5">
          <div className="max-w-4xl mx-auto text-center">
            <Link href="/">
              <a>
                <div className="h-8 w-auto sm:h-10">
                  <Image
                    src="/images/gitlab-logo-white.svg"
                    alt="GitLab logo"
                    width={95}
                    height={20}
                  />
                </div>
              </a>
            </Link>
            <p>&copy; {new Date().getFullYear()} GitLab, Inc.</p>
          </div>
      </footer>
    </div>
  )
}
