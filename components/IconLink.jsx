import Link from 'next/link'
import IconFeed from './icons/IconFeed'
import IconGithub from './icons/IconGithub'
import IconLinkedin from './icons/IconLinkedin'
import IconSlack from './icons/IconSlack'
import IconTwitter from './icons/IconTwitter'

export default function IconLink({
  name,
  url,
  width = '16',
  height = '16',
  className = 'text-indigo-500 hover:text-indigo-700'
}) {
  const isTwitter = name === 'twitter'
  const isLinkedin = name === 'linkedin'
  const isGithub = name === 'github'
  const isSlack = name === 'slack'
  const isFeed = name === 'feed'
  const iconNames = ['twitter', 'linkedin', 'github', 'slack', 'feed']

  if (!iconNames.includes(name)) return null

  return (
    <Link href={url}>
      <a className={className} title={name}>
        {isTwitter ? <IconTwitter width={width} height={height} /> : null}
        {isLinkedin ? <IconLinkedin width={width} height={height} /> : null}
        {isGithub ? <IconGithub width={width} height={height} /> : null}
        {isSlack ? <IconSlack width={width} height={height} /> : null}
        {isFeed ? <IconFeed width={width} height={height} /> : null}
      </a>
    </Link>
  )
}
