import Link from 'next/link'

export default function LinkButton({ text, href, className }) {
  return (
    <Link href={href}>
      <a className={`group flex items-center space-x-2 text-lg text-black font-semibold hover:text-black ${className}`}>
        <span>{text}</span>
        <span className="group-hover:translate-x-5">
          <svg
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M9 18L15 12L9 6"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
        </span>
      </a>
    </Link>
  )
}
