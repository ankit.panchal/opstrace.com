import Link from 'next/link'
import Image from 'components/Image'
import ProfilePic from 'components/blog/ProfilePic'
import { team as authors } from '../content/team'

export default function BlogCard({ post }) {
  const author = authors[post.authors[0]]
  let authorName = author.name
  if (post.authors.length > 1) {
    authorName += ', et al.'
  }

  return (
    <div className="flex flex-col rounded-lg shadow-lg overflow-hidden">
      <div className="flex-shrink-0">
        <Link href={post.slug}>
          <a>
            <Image
              className="h-48 w-full object-cover"
              width={500}
              height={192}
              src={post.featuredImage.src}
              alt={post.featuredImage.alt}
            />
          </a>
        </Link>
      </div>
      <div className="flex-1 bg-white p-6 flex flex-col justify-between">
        <div className="flex-1">
          <p className="text-sm font-medium text-indigo-600">
            <Link href={post.slug}>
              <a className="hover:underline">{post.type}</a>
            </Link>
          </p>
          <Link href={post.slug}>
            <a className="block mt-2">
              <p className="text-xl font-semibold text-gray-900">
                {post.title}
              </p>
              <p className="mt-3 text-base text-gray-500">{post.description}</p>
            </a>
          </Link>
        </div>
        <div className="mt-6 flex items-center">
          <div className="flex-shrink-0">
            <span className="sr-only">{authorName}</span>
            <ProfilePic
              src={`/images/team/${author.picture}`}
              alt={`portrait of ${author.name}`}
            />
          </div>
          <div className="ml-3">
            <p className="text-sm font-medium text-gray-900">{authorName}</p>
            <div className="flex space-x-1 text-sm text-gray-500">
              <time dateTime={post.semanticDate}>{post.humanDate}</time>
              <span aria-hidden="true">&middot;</span>
              <span>{post.readingTime.text}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
