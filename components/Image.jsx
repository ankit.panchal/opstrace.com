import Image from 'next/image'

export default function Img(props) {
  return <Image unoptimized={true} {...props} src={process.env.NODE_ENV === 'production' ? `/opstrace.com${props.src}` : props.src}/>
}
