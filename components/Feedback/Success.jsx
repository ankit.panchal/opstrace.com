const Success = () => (
  <div className="text-center">
    <p>Your feedback has been received!</p>

    <p>Thank you for your help.</p>
  </div>
)

export default Success
