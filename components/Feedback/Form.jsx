import React, { useState } from 'react'
import Spinner from './Spinner'

const Form = ({ onSubmit, loading, className }) => {
  const [form, setForm] = useState({
    email: '',
    note: ''
  })

  const onFormSubmit = (e) => {
    e.preventDefault()
    onSubmit(form)
  }

  const onFormChange = ({ target }) => {
    setForm((prev) => ({
      ...prev,
      [target.name]: target.value
    }))
  }

  return (
    <form
      onChange={onFormChange}
      onSubmit={onFormSubmit}
      className={`feedback-form overflow-hidden px-1 ${className}`}
    >
      <div className="mb-4">
        <label
          htmlFor="email"
          className="block text-sm font-medium text-gray-700 mb-2"
        >
          Email (optional)
        </label>
        <input
          disabled={loading}
          autoComplete="off"
          id="email"
          name="email"
          type="email"
          className="shadow-sm focus:ring-opstrace-500 focus:border-opstrace-500 block w-full sm:text-sm border-gray-300 rounded-md"
          defaultValue={form.email}
          placeholder="Email address"
        />
      </div>
      <div className="mb-4">
        <label
          htmlFor="feedback"
          className="block text-sm font-medium text-gray-700 mb-2"
        >
          Feedback (optional)
        </label>
        <textarea
          disabled={loading}
          autoComplete="off"
          id="feedback"
          name="note"
          className="shadow-sm h-32 resize-none focus:ring-opstrace-500 focus:border-opstrace-500 block w-full sm:text-sm border-gray-300 rounded-md"
          defaultValue={form.note}
          placeholder="Feedback"
        />
      </div>
      <div className="flex justify-end w-full">
        <button
          disabled={loading}
          type="submit"
          className="h-9 w-20 border border-transparent text-base font-medium items-center rounded-md flex justify-center transition text-white bg-pink-600 hover:bg-pink-700"
        >
          {loading ? <Spinner /> : 'Send'}
        </button>
      </div>
    </form>
  )
}

export default Form
