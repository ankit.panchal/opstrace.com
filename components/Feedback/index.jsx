import React, { useState, useRef, useEffect } from 'react'
import { useRouter } from 'next/router'
import Form from './Form'
import Success from './Success'
import { event } from 'lib/ga'

const emojiArr = ['😭', '😕', '🙂', '🤩']

const sendFeedback = async (formData) => {
  const res = await fetch(
    'https://us-central1-opstrace-production.cloudfunctions.net/feedback',
    {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(formData)
    }
  )
  return res.json()
}

const Feedback = () => {
  const router = useRouter()
  const componentRef = useRef()
  const [emoji, setEmoji] = useState(null)
  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)

  useEffect(() => {
    document.addEventListener('mousedown', handleClick)
    return () => document.removeEventListener('mousedown', handleClick)
    function handleClick(e) {
      event({
        action: 'submit',
        category: 'feedback',
        label: router.asPath,
        value: e.val
      })
      if (componentRef && componentRef.current) {
        const ref = componentRef.current
        if (!ref.contains(e.target)) {
          setEmoji(null)
        }
      }
    }
  }, [])

  const onSubmit = async (formData) => {
    const submittedData = {
      ...formData,
      label: 'docs',
      url: router.asPath.split('#')[0],
      ua: navigator.userAgent,
      emoji
    }

    try {
      setLoading(true)
      await sendFeedback(submittedData)
      setSuccess(true)
    } catch (error) {
      console.error(error)
    }
    setLoading(false)
  }

  const onEmojiClick = (val) => {
    event({
      action: 'emoji click',
      category: 'feedback',
      label: router.asPath,
      value: val
    })
    setEmoji(val)
    setSuccess(false)
  }

  return (
    <div className="feedback-wrapper py-10 my-10 border-b border-t border-gray-200">
      <h3 className="text-center w-full d-block font-bold text-xl mb-4">
        Was this helpful?
      </h3>
      <div ref={componentRef}>
        <div className="flex items-center justify-center  mb-4">
          {emojiArr.map((emoj) => (
            <button
              key={emoj}
              onClick={() => onEmojiClick(emoj)}
              className={`focus:outline-none mx-1 text-xl hover:text-2xl w-10 h-10 emoji ${
                emoj === emoji ? 'active' : ''
              }`}
            >
              <span>{emoj}</span>
            </button>
          ))}
        </div>
        <div className={`form-wrapper ${emoji ? 'focused' : ''}`}>
          {success ? (
            <Success />
          ) : (
            <Form onSubmit={onSubmit} loading={loading} />
          )}
        </div>
      </div>
    </div>
  )
}

export default Feedback
