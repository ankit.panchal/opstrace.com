import Image from 'components/Image'

export default function Testimonial({ testimonial }) {
  return (
    <section className="py-12 bg-gray-50 overflow-hidden md:py-20 lg:py-24">
      <div className="relative max-w-screen-xl mx-auto px-4 sm:px-6 lg:px-8">
        <Image
          className="mx-auto object-fit object-center h-24 w-24"
          src={testimonial.logo}
          alt={testimonial.company}
          width="200"
          height="100"
          layout="responsive"
        />
        <div className="relative">
          <blockquote className="mt-8">
            <div className="max-w-3xl mx-auto text-center text-2xl leading-9 font-medium text-gray-900">
              <p>{testimonial.text}</p>
            </div>
            <footer className="mt-8">
              <div className="md:flex md:items-center md:justify-center">
                <div className="md:flex-shrink-0">
                  <Image
                    className="mx-auto h-10 w-10 rounded-full"
                    src={testimonial.author.image}
                    alt={testimonial.author.name}
                    width="200"
                    height="100"
                    layout="responsive"
                  />
                </div>
                <div className="mt-3 text-center md:mt-0 md:ml-4 md:flex md:items-center">
                  <div className="text-base leading-6 font-medium text-gray-900">
                    {testimonial.author.name}
                  </div>

                  <svg
                    className="hidden md:block mx-1 h-5 w-5 text-pink-600"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                  >
                    <path d="M11 0h3L9 20H6l5-20z" />
                  </svg>

                  <div className="text-base leading-6 font-medium text-gray-500">
                    {testimonial.author.title}
                  </div>
                </div>
              </div>
            </footer>
          </blockquote>
        </div>
      </div>
    </section>
  )
}
