import Link from 'next/link'
import Image from 'components/Image'

const Navigation = () => {
  return (
    <div className="container py-5 absolute z-10 left-0 right-0 top-0">
      <div className="flex flex-wrap items-center justify-between">
        <div className="">
          <Link href="/">
            <a>
              <div className="h-8 w-auto sm:h-10">
                <Image
                  src="/images/gitlab-logo.svg"
                  alt="GitLab logo"
                  width={158}
                  height={34}
                />
              </div>
            </a>
          </Link>
        </div>
        <nav>
          <Link href="/docs">
            <a className="block px-4 py-2 bg-black text-white font-semibold text-lg rounded flex space-x-2 items-center border border-black hover:bg-transparent hover:text-black transition">
              <span>Docs</span>
              <span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                >
                  <path
                    d="M7 17L17 7"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M7 7H17V17"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </span>
            </a>
          </Link>
        </nav>
      </div>
    </div>
  )
}

export default Navigation
