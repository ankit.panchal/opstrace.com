import Link from 'next/link'
import Image from 'components/Image'
import Authors from 'components/blog/Authors'
import authors from 'content/team'

export default function BlogCard({ post, className = '' }) {
  const postAuthors = post.authors.map((key) => authors[key])

  return (
    <Link href={post.slug}>
      <a className={`block md:grid grid-cols-5 gap-20 ${className}`}>
        <div className="col-start-1 col-end-4 row-start-1 mb-5">
          <span className="text-indigo-500 font-bold uppercase tracking-widest mb-1">
            {post.type}
          </span>
          <h2 className="text-3xl text-blue-700 mb-1">{post.title}</h2>
          <div className="text-sm text-blue-650 font-medium mb-5">
            <time dateTime={post.semanticDate}>{post.humanDate}</time>
            <span aria-hidden="true" className="mx-2">
              &#124;
            </span>
            <span>{post.readingTime.text}</span>
          </div>
          <p className="text-lg font-medium text-gray-700 mb-4">
            {post.description}
          </p>
          <Authors authors={postAuthors} />
        </div>
        <div className="col-start-4 col-end-6 self-start rounded-md overflow-hidden shadow-lg">
          <Image
            className="object-cover"
            layout="responsive"
            width={600}
            height={314}
            src={post.featuredImage.src}
            alt={post.featuredImage.alt}
          />
        </div>
      </a>
    </Link>
  )
}
