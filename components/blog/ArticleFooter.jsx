import IconLink from 'components/IconLink'
import Link from 'next/link'

export default function ArticleFooter({ currentPath, recommended }) {
  return (
    <div className="mt-32 px-4 xl:px-12 py-5 xl:py-16 bg-gray-100 rounded-xl">
      <h3 className="text-3xl font-medium text-black mb-6">
        You might also like...
      </h3>
      <ul className="mb-10">
        {recommended.map((post) => (
          <li className="mb-4" key={post.slug}>
            <Link href={post.slug}>
              <a
                className={`text-gray-700 hover:text-pink-400 ${
                  post.slug === currentPath ? 'font-bold' : ''
                }`}
              >
                {post.title}
              </a>
            </Link>
          </li>
        ))}
      </ul>
      <div className="md:flex justify-between items-baseline border-t-2 border-gray-100 pt-5 space-y-4">
        <p className="text-xl font-semibold">Follow Opstrace on...</p>
        <div className="flex items-end space-x-4">
          <IconLink
            width="24"
            height="24"
            name="twitter"
            url="https://twitter.com/opstrace"
            className="text-black"
          />
          <IconLink
            width="24"
            height="24"
            name="github"
            url="https://github.com/opstrace/opstrace"
            className="text-black"
          />
          <IconLink
            width="24"
            height="24"
            name="linkedin"
            url="https://www.linkedin.com/company/opstrace/"
            className="text-black"
          />
          <IconLink
            width="24"
            height="24"
            name="slack"
            url="https://go.opstrace.com/community"
            className="text-black"
          />
        </div>
      </div>
    </div>
  )
}
