import Image from '../Image'
import Authors from 'components/blog/Authors'

export default function ArticleHeader({ meta, authors, className = '' }) {
  return (
    <header className={className}>
      <p className="text-sm text-gray-600 mb-3">
        {meta.humanDate} <span className="inline-block mx-1">&#124;</span>{' '}
        {meta.readingTime.text}
      </p>
      <h1 className="font-semibold leading-snug text-5xl mb-5 text-black">
        {meta.title}
      </h1>
      <Authors className="mb-6 xl:hidden" authors={authors} expanded={true} />
      <div className="shadow-lg mb-12 rounded overflow-hidden">
        <Image
          className="w-full object-cover"
          src={meta.featuredImage.src}
          alt={meta.featuredImage.alt}
          width="1200"
          height="628"
          layout="responsive"
        />
      </div>
      <p className="font-sans text-2xl mb-16">{meta.description}</p>
    </header>
  )
}
