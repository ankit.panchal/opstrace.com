import Link from 'next/link'
import { useRouter } from 'next/router'

export default function RecommendedArticles({ posts, className = '' }) {
  const router = useRouter()
  const currentPath = router.asPath

  return (
    <div className={className}>
      <h3 className="text-lg font-bold mb-4">Recommended Posts</h3>
      <ul>
        {posts.map((post) => (
          <li className="mb-4" key={post.slug}>
            <Link href={post.slug}>
              <a
                className={`text-gray-700 hover:text-pink-400 text-sm ${
                  post.slug === currentPath ? 'font-bold' : ''
                }`}
              >
                {post.title}
              </a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  )
}
