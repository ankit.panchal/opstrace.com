import ProfilePic from './ProfilePic'
import IconLink from 'components/IconLink'

export default function Authors({ authors, expanded = false, className = '' }) {
  if (expanded) {
    return authors.map((author) => (
      <div className={className} key={author.id}>
        <div className="flex items-start mb-8">
          <ProfilePic
            className="block mr-2"
            src={`/images/team/${author.picture}`}
            alt={`portrait of ${author.name}`}
          />
          <div className="font-medium text-sm text-blue-650">
            <p>{author.name}</p>
            <p>{author.description}</p>
            <SocialIcons links={author.links} className="mt-3" />
          </div>
        </div>
      </div>
    ))
  } else {
    return (
      <div className={className}>
        <div className="flex -space-x-3">
          {authors.map((author) => {
            return (
              <ProfilePic
                className=""
                src={`/images/team/${author.picture}`}
                alt={`portrait of ${author.name}`}
                key={author.id}
              />
            )
          })}
        </div>
      </div>
    )
  }
}

function SocialIcons({ links, className = '' }) {
  if (!links || links === undefined) {
    return null
  }
  return (
    <div className={`flex items-center space-x-2 ${className}`}>
      {Object.entries(links).map((entry) => {
        const [name, url] = entry
        return (
          <IconLink
            key={url}
            name={name}
            url={url}
            className="text-gray-700 hover:text-gray-900"
          />
        )
      })}
    </div>
  )
}
