import Image from 'components/Image'

export default function ProfilePic({ src, alt, className = '' }) {
  return (
    <div className={`h-10 w-10 ${className}`}>
      <Image
        className={`h-10 w-10 rounded-full`}
        width="40"
        height="40"
        src={src}
        alt={alt}
      />
    </div>
  )
}
