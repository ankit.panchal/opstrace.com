import Link from 'next/link'
import Image from 'components/Image'
import LinkButton from './LinkButton'

export default function BlogCard({ post }) {
  return (
    <div className='flex flex-col px-5 py-8 hover:bg-indigo-50 hover:rounded-br-3xl transition'>
      <Link href={post.slug}>
        <a className='text-black hover:text-black'>
          <span className='text-xs uppercase opacity-60'>{post.type || 'Article'}</span>
          <div className='overflwo-hidden'>
            <Image
              className="object-cover"
              layout="responsive"
              width="370"
              height="190"
              src={post.featuredImage.src}
              alt={post.featuredImage.alt}
            />

          </div>
          <h2 className="text-xl my-4">
            {post.title}
          </h2>

        </a>
      </Link>
      <LinkButton href={post.slug} text="Read more" className="mt-auto" />
    </div>
  )
}
