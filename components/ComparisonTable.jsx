import Link from 'next/link'

export default function ComparisonTable({ table }) {
  const icon = (isIncluded) => {
    return isIncluded ? (
      <svg
        className="h-5 w-5 text-green-500"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
        aria-hidden="true"
      >
        <path
          fillRule="evenodd"
          d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
          clipRule="evenodd"
        />
      </svg>
    ) : (
      <svg
        className="h-5 w-5 text-pink-800"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
        aria-hidden="true"
      >
        <path
          fillRule="evenodd"
          d="M3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
          clipRule="evenodd"
        />
      </svg>
    )
  }

  return (
    <div className="bg-white">
      <div className="max-w-7xl mx-auto bg-white py-16 sm:py-24">
        <h2 className="px-4 text-3xl mt-10 font-extrabold text-gray-900 sm:text-4xl mb-14">
          How does Opstrace compare?
        </h2>
        {/* <!-- xs to lg --> */}
        <div className="lg:hidden">
          {table.columns.map((column, cidx) => {
            return (
              <div key={`m-col-${cidx}`} className="max-w-2xl mx-auto">
                <div className="px-4">
                  <h2 className="text-3xl leading-6 font-medium text-gray-900">
                    {column.title}
                  </h2>
                  <p className="mt-4 text-sm text-gray-500">
                    {column.description}
                  </p>
                </div>

                {table.groups.map((group, gidx) => {
                  return (
                    <table
                      key={`m-group-${gidx}`}
                      className="mt-8 w-full table-fixed"
                    >
                      <caption className="bg-gray-50 border-t border-gray-200 py-3 px-4 text-sm font-medium text-gray-900 text-left">
                        {group.title}
                      </caption>
                      <tbody className="divide-y divide-gray-200">
                        {group.features.map((feature, fidx) => {
                          const columnId = column.title.toLowerCase()
                          const isIncluded = feature[columnId] === true
                          const icn = icon(isIncluded)

                          return (
                            <tr
                              key={`m-feature-${gidx}-${fidx}`}
                              className="w-full border-t border-gray-200"
                            >
                              <th
                                className="w-5/6 py-5 px-4 text-xl font-normal text-gray-500 text-left"
                                scope="row"
                              >
                                {feature.feature}
                              </th>
                              <td className="w-1/6 py-5 pr-4 text-right">
                                {icn}
                                <span className="sr-only">
                                  {isIncluded ? 'Yes' : 'No'}
                                </span>
                              </td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  )
                })}

                <p className="mt-4 px-4 mb-16">
                  <span className="text-xl text-gray-900">
                    Pricing per {column.unit}
                  </span>
                  <span className="text-base font-medium text-gray-500">
                    /mo
                  </span>
                </p>
              </div>
            )
          })}
          <div className="px-4">
            {/* eslint react/jsx-no-target-blank: "off" */}
            <Link
              href="https://opstrace.typeform.com/to/csRIBbw3"
              target="_blank"
              rel="noopener"
              className="w-full block border rounded-md py-6 text-xl font-semibold text-white text-center bg-pink-600 hover:bg-pink-400 active:bg-pink-800"
            >
              <a>Request a Demo</a>
            </Link>
          </div>
        </div>

        {/* <!-- lg+ --> */}
        <div className="hidden lg:block">
          <table className="w-full h-px table-fixed">
            <caption className="sr-only">{table.title}</caption>
            <thead>
              <tr>
                <th
                  className="w-1/2 pb-4 px-6 text-2xl font-medium text-gray-900 text-left"
                  scope="col"
                ></th>

                {table.columns.map((column) => {
                  const colSize = table.columns.length + 1

                  return (
                    <th
                      key={column.title.toLowerCase()}
                      className={`w-1/${colSize} pb-4 px-6 text-2xl leading-6 font-medium text-gray-900 text-left`}
                      scope="col"
                    >
                      {column.title}
                    </th>
                  )
                })}
              </tr>
            </thead>
            {table.groups.map((group, gidx) => {
              return (
                <tbody
                  key={`group-${gidx}`}
                  className="border-t border-gray-200 divide-y divide-gray-200"
                >
                  <tr>
                    <th
                      className="bg-gray-50 py-3 pl-6 text-2xl font-medium text-gray-900 text-left"
                      colSpan="4"
                      scope="colgroup"
                    >
                      {group.title}
                    </th>
                  </tr>
                  {group.features.map((feature, fidx) => {
                    return (
                      <tr key={`feature-${gidx}-${fidx}`}>
                        <th
                          className="py-5 px-6 text-xl font-normal text-gray-500 text-left"
                          scope="row"
                        >
                          {feature.feature}
                        </th>
                        {table.columns.map((column, cidx) => {
                          const columnId = column.title.toLowerCase()
                          const isIncluded = feature[columnId] === true
                          const icn = icon(isIncluded)
                          return (
                            <td
                              key={`feature-included-${gidx}-${fidx}-${cidx}`}
                              className="py-5 px-6"
                            >
                              {icn}
                              <span className="sr-only">
                                Included in {column.title}
                              </span>
                            </td>
                          )
                        })}
                      </tr>
                    )
                  })}
                </tbody>
              )
            })}
            <tfoot>
              <tr className="">
                <td colSpan="4" className="pt-5 px-6">
                  <div className="flex justify-center pt-24">
                    {/* eslint react/jsx-no-target-blank: "off" */}
                    <Link
                      href="https://opstrace.typeform.com/to/csRIBbw3"
                      target="_blank"
                      rel="noopener"
                      className="w-1/3 block border rounded-md py-6 text-2xl font-semibold text-white text-center bg-pink-600 hover:bg-pink-400 active:bg-pink-800 hover:text-white"
                    >
                      <a>Request a Demo</a>
                    </Link>
                  </div>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  )
}
