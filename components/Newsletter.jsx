import { useState } from 'react'
// import Link from 'next/link'
import MailchimpSubscribe from 'react-mailchimp-subscribe'

export default function Newsletter({ content }) {
  const [emailInput, setEmailInput] = useState('')
  const url =
    content.url ||
    'https://opstrace.us17.list-manage.com/subscribe/post?u=256e1821e5dca1dfd88cc4372&amp;id=df70d5e8aa'

  return (
    <MailchimpSubscribe
      url={url}
      render={({ subscribe, status }) => {
        const subscribedSuccessfully = status === 'success'
        const buttonText = subscribedSuccessfully ? 'Thank you' : 'Notify me'
        const handleClick = (event) => {
          event.preventDefault()
          subscribe({
            EMAIL: emailInput
          })
        }
        if (content.version === 'huge') {
          return (
            <section
              id="newsletter"
              className="newsletter bg-special-100 py-8 bg-bubblesBw"
            >
              <div className="flex flex-col items-center max-w-7xl mx-auto py-12 px-4 sm:bg-tracyThinking bg-no-repeat bg-contain bg-right">
                <div className="w-full">
                  <h3
                    className="font-black mt-12 text-5xl text-center text-white"
                    id="newsletter-headline"
                  >
                    {content.title}
                  </h3>
                  <p className="mt-3 text-lg text-center text-gray-200">
                    {content.text}
                  </p>
                </div>
                <div className="my-12">
                  <form className="flex justify-center">
                    <label htmlFor="emailAddress" className="sr-only">
                      Email address
                    </label>
                    <input
                      id="emailAddress"
                      value={emailInput}
                      onChange={(e) => setEmailInput(e.target.value)}
                      disabled={subscribedSuccessfully}
                      type="email"
                      required
                      className="w-full px-5 py-5 border border-transparent placeholder-gray-500 focus:ring-2 focus:ring-offset-0 focus:ring-offset-white focus:ring-white focus:border-white sm:max-w-xs rounded-md"
                      placeholder="Enter your email"
                    />
                    <div className="mt-0 ml-1 rounded-md shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
                      <button
                        type="submit"
                        onClick={handleClick}
                        className="w-full flex items-center justify-center px-5 py-5 border border-transparent text-base font-medium rounded-md text-white bg-pink-600 hover:bg-pink-400 active:bg-pink-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-pink-500"
                      >
                        {buttonText}
                      </button>
                    </div>
                  </form>
                  <p className="text-center mt-3 text-sm text-gray-300">
                    We won&apos;t share your information with anyone.
                    {/* Read our&nbsp;
                    <Link href="/privacy-policy">
                      <a className="text-white font-medium underline">
                        Privacy Policy.
                      </a>
                    </Link> */}
                  </p>
                </div>
              </div>
            </section>
          )
        } else if (content.version === 'footer') {
          return (
            <div className="">
              <h2 className="text-white font-bold text-2xl mb-4 lg:mb-6">
                {content.title}
              </h2>
              <div className="md:flex md:space-x-4">
                <form className="flex lg:w-2/3">
                  <label htmlFor="emailAddress" className="sr-only">
                    Email address
                  </label>
                  <input
                    id="emailAddress"
                    value={emailInput}
                    onChange={(e) => setEmailInput(e.target.value)}
                    disabled={subscribedSuccessfully}
                    type="email"
                    required
                    className="h-10 w-full rounded-l-xl text-blue-800"
                    placeholder="Enter your email"
                  />
                  <button
                    type="submit"
                    onClick={handleClick}
                    className="block h-10 w-10 rounded-r-xl bg-gradient-to-r from-pink-500 to-pink-800"
                  >
                    <svg
                      className="mx-auto"
                      width="12"
                      height="20"
                      viewBox="0 0 12 20"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M1.625 2.27734L9.625 10.2773L1.625 18.2773"
                        stroke="white"
                        strokeWidth="3"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      />
                    </svg>
                  </button>
                </form>
                <p className="text-sm mt-2 lg:mt-0">{content.text}</p>
              </div>
            </div>
          )
        } else {
          return (
            <section
              id="newsletter-dark"
              className="newsletter bg-opstrace-800"
            >
              <div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center">
                <div className="lg:w-0 lg:flex-1">
                  <h2
                    className="text-3xl font-extrabold tracking-tight text-white sm:text-4xl"
                    id="newsletter-headline"
                  >
                    {content.title}
                  </h2>
                  <p className="mt-3 max-w-3xl text-lg leading-6 text-gray-300">
                    {content.text}
                  </p>
                </div>
                <div className="mt-8 lg:mt-0 lg:ml-8">
                  <form className="sm:flex">
                    <label htmlFor="emailAddress" className="sr-only">
                      Email address
                    </label>
                    <input
                      id="emailAddress"
                      value={emailInput}
                      onChange={(e) => setEmailInput(e.target.value)}
                      disabled={subscribedSuccessfully}
                      type="email"
                      required
                      className="w-full px-5 py-3 border border-transparent placeholder-gray-500 focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white sm:max-w-xs rounded-md"
                      placeholder="Enter your email"
                    />
                    <div className="mt-3 rounded-md shadow sm:mt-0 sm:ml-3 sm:flex-shrink-0">
                      <button
                        type="submit"
                        onClick={handleClick}
                        className="w-full flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-opstrace-500 hover:bg-opstrace-600 active:bg-pink-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-opstrace-500"
                      >
                        {buttonText}
                      </button>
                    </div>
                  </form>
                  <p className="mt-3 text-sm text-gray-300">
                    We won&apos;t share your information with anyone.
                    {/* Read our&nbsp;
                    <Link href="/privacy-policy">
                      <a className="text-white font-medium underline">
                        Privacy Policy.
                      </a>
                    </Link> */}
                  </p>
                </div>
              </div>
            </section>
          )
        }
      }}
    />
  )
}
