import { format, formatDistance } from 'date-fns'

const importAll = (context) => {
  const keys = context.keys()
  const values = keys.map(context)

  const data = keys
    .map((key, index) => {
      const value = values[index]
      const meta = value.meta
      meta.slug = `/media/${key.replace(/^.*[\\/]/, '').slice(0, -4)}`
      meta.id = key.replace(/^.*[\\/]/, '').slice(0, -4)
      meta.humanDate = format(new Date(meta.date), 'MMM d')
      meta.relativeDate = formatDistance(new Date(meta.date), new Date(), {
        addSuffix: true
      })
      meta.semanticDate = format(new Date(meta.date), 'yyyy-MM-dd')
      return { ...meta, module: context(key) }
    })
    .sort(function (a, b) {
      return new Date(b.date) - new Date(a.date)
    })
  return data
}

export const media = importAll(
  require.context('content/media/', true, /\.\/.*\.mdx$/)
)
