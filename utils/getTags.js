const getTags = (context) => {
  const keys = context.keys()
  const values = keys.map(context)
  const data = keys.map((key, index) => {
    const { meta } = values[index]
    return meta.tags
  })
  return data.flat().sort()
}
export default getTags
