import { format } from 'date-fns'

const getClosestPosts = (context, current) => {
  const closest = {}
  const keys = context.keys()
  const values = keys.map(context)

  const data = keys
    .map((key, index) => {
      const { meta } = values[index]
      const slug = `/blog/${key.replace(/^.*[\\/]/, '').slice(0, -4)}`
      meta.slug = slug
      meta.humanDate = format(new Date(meta.date), 'MMM d')
      meta.semanticDate = format(new Date(meta.date), 'yyyy-MM-dd')
      return { ...meta }
    })
    .sort(function (a, b) {
      return new Date(b.date) - new Date(a.date)
    })

  const idx = data.findIndex((meta) => {
    return meta.date === current
  })

  if (idx > 0) {
    closest.next = data[idx - 1]
  }

  if (idx < data.length - 1) {
    closest.previous = data[idx + 1]
  }

  return closest
}

export default getClosestPosts
