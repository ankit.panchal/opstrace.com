const getSlugs = (context) => {
  const keys = context.keys()

  const data = keys.map((key) => {
    const [slug] = key.replace(/^.*[\\/]/, '').split('.')

    return slug
  })
  return data
}

export default getSlugs
