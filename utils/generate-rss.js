import fs from 'fs'
import { Feed } from 'feed'
import getPosts from 'utils/getPosts'
import meta from 'content/site.js'

async function generate() {
  const feed = new Feed({
    title: meta.title,
    description: meta.description,
    image: meta.image,
    favicon: meta.favicon,
    copyright: meta.copyright,
    language: meta.language,
    link: meta.link,
    id: 'https://opstrace.com/',
    feedLinks: {
      json: `${meta.link}feed.json`,
      rss2: `${meta.link}feed.xml`,
      atom: `${meta.link}atom.xml`
    }
  })

  const posts = ((context) => {
    return getPosts(context)
  })(require.context('content/articles', true, /\.\/.*\.mdx$/))

  posts.forEach((post) => {
    feed.addItem({
      title: post.title,
      id: `${meta.link}${post.slug}`,
      link: `${meta.link}${post.slug}`,
      date: new Date(post.date),
      description: post.description,
      image: `${meta.link}${post.featuredImage.src}`
    })
  })

  fs.writeFileSync('./public/feed.xml', feed.rss2())
  fs.writeFileSync('./public/feed.json', feed.json1())
  fs.writeFileSync('./public/atom.xml', feed.atom1())
}

generate()
