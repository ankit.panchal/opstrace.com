import { format } from 'date-fns'

const getPosts = (context) => {
  const keys = context.keys()
  const values = keys.map(context)
  const data = keys
    .map((key, index) => {
      const slug = `/blog/${key.replace(/^.*[\\/]/, '').slice(0, -4)}`
      const { meta } = values[index]
      meta.id = key.replace(/^.*[\\/]/, '').slice(0, -4)
      meta.humanDate = format(new Date(meta.date), 'MMM d')
      meta.semanticDate = format(new Date(meta.date), 'yyyy-MM-dd')
      return { ...meta, slug }
    })
    .sort(function (a, b) {
      return new Date(b.date) - new Date(a.date)
    })
  return data
}
export default getPosts
