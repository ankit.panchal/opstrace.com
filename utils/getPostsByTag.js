const getPostsByTag = (context, tag) => {
  const keys = context.keys()
  const values = keys.map(context)
  const data = keys
    .map((key, index) => {
      const slug = `/blog/${key.replace(/^.*[\\/]/, '').slice(0, -4)}`
      const { meta } = values[index]
      return { ...meta, slug }
    })
    .filter((post) => post.tags.includes(tag))
    .sort((a, b) => {
      return new Date(b.date) - new Date(a.date)
    })
  return data
}
export default getPostsByTag
